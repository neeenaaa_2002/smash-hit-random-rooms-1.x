# Smash Hit Random Rooms 1.x

This is the repository for Cdde's Smash Hit Random Rooms mod, for v1.9.1 and later. This repo only contains the `assets` folder, so you'll need to get the rest of the APK elsewhere. If you'd like to contribute, you can do that.

## Setup

You need to delete the exsisting assets folder and then copy this one onto the APK.

### Important Files

* `assets/rooms/random/random.lua` - the main random room file

## Mod Info

This mod is based on 1.4.x, specifically 1.4.2 and 1.4.3.

## File License

The files that the mod are based on are (C) Mediocre AB 2014 - 2019.
